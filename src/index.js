const mongoose = require('mongoose');
const express = require('express');
const app = express();
const port = 3000;

app.use(express.json())

const cours = require('./routes/cours');
const ecoles = require('./routes/ecole');
const proprietaires = require('./routes/proprietaire');
const sessions = require('./routes/session');

main().catch(err => console.log(err));

async function main() {
    await mongoose.connect('mongodb://127.0.0.1:27017/coursCanin');
    console.log("Connected to the database");

    app.get('/', async (req, res) => {
        res.send("Welcome the the REST API");
    });

    app.use('/cours', cours);
    app.use('/ecoles', ecoles);
    app.use('/proprietaires', proprietaires);
    app.use('/sessions', sessions);

    app.listen(port, () => {
        console.log(`App listening on port ${port} => http://localhost:${port}`);
    });
}