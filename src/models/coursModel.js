const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const coursSchema = new Schema({
    "niveau": String,
    "tarif": Number,
    "type": { type: String },
}, { collection: "cours" })

module.exports = model("cours", coursSchema);