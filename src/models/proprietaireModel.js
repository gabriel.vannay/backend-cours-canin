const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const proprietaireSchema = new Schema({
    "adresse": String,
    "cerficatDelivre": Date,
    "dateNaissance": Date,
    "nom": String,
    "prenom": String,
    "origine": String,
    "localite": String,
    "npa": Number,
    "chiens": [
        {
            "nom": String,
            "male": Boolean,
            "identificationAmicus": String,
            "dateNaissance": String,
            "race": String,
        }
    ],
    "mails": [
        {
            "mail": String,
            "type": { type: String }
        }
    ],
    "telephones": [
        {
            "numero": String,
            "type": { type: String }
        }
    ],
}, { collection: "proprietaire" })

module.exports = model("proprietaire", proprietaireSchema);