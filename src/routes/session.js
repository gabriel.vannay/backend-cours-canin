const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const ObjectId = mongoose.Types.ObjectId;

const sessionModel = require('../models/sessionModel');

router.get('/', async (req, res) => {
    const session = await sessionModel.find();
    res.send(session);
})

router.get('/:id', async (req, res) => {
    const id = new ObjectId(req.params.id);
    const session = await sessionModel.aggregate([
        {
            $match: { _id: id }
        },
        {
            $unwind: "$participations"
        },
        {
            $lookup: {
                from: "proprietaire",
                let: { chienId: "$participations.chien_id" },
                pipeline: [
                    {
                        $unwind: "$chiens"
                    },
                    {
                        $match: {
                            $expr: {
                                $eq: ["$$chienId", "$chiens._id"]
                            }
                        }
                    },
                    {
                        $project: {
                            "_id": "$chiens._id",
                            "nom": "$chiens.nom",
                            "male": "$chiens.male",
                            "identificationAmicus": "$chiens.identificationAmicus",
                            "dateNaissance": "$chiens.dateNaissance",
                            "race": "$chiens.race",
                            "proprietaire._id": "$_id"
                        }
                    }
                ],
                as: "chien"
            }
        },
        {
            $project: {
                "date": "$date",
                "dureeM": "$dureeM",
                "places": "$places",
                "avecProprietaire": "$participations.avecProprietaire",
                "cours_id": "$cours_id",
                "moniteur_id": "$moniteur_id",
                "chien": "$chien"
            }
        },
        {
            $unwind: "$chien"
        }
    ]);
    res.send(session);
});

router.post('/', async (req, res) => {
    const insert = await sessionModel.insertMany(req.body);
    res.send(insert);
});

router.put('/:id', async (req, res) => {
    const update = await sessionModel.updateOne(
        { _id: req.params.id },
        req.body
    );
    res.send(update);
});

router.delete('/:id', async (req, res) => {
    const deleteRes = await sessionModel.deleteOne({ _id: req.params.id });
    res.send(deleteRes);
});

module.exports = router;