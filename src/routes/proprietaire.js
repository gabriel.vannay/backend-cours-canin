const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

const proprietaireModel = require('../models/proprietaireModel');

router.get('/', async (req, res) => {
    const proprietaire = await proprietaireModel.find();
    res.send(proprietaire);
})

router.get('/:id', async (req, res) => {
    const proprietaire = await proprietaireModel.findOne({ _id: req.params.id });
    res.send(proprietaire);
});

router.post('/', async (req, res) => {
    const insert = await proprietaireModel.insertMany(req.body);
    res.send(insert);
});

router.put('/:id', async (req, res) => {
    const update = await proprietaireModel.updateOne(
        { _id: req.params.id },
        req.body
    );
    res.send(update);
});

router.delete('/:id', async (req, res) => {
    const deleteRes = await proprietaireModel.deleteOne({ _id: req.params.id });
    res.send(deleteRes);
});

module.exports = router;