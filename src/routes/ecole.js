const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

const ecoleModel = require('../models/ecoleModel');

router.get('/', async (req, res) => {
    const ecole = await ecoleModel.find();
    res.send(ecole);
})

router.get('/:id', async (req, res) => {
    const ecole = await ecoleModel.findOne({ _id: req.params.id });
    res.send(ecole);
});

router.post('/', async (req, res) => {
    const insert = await ecoleModel.insertMany(req.body);
    res.send(insert);
});

router.put('/:id', async (req, res) => {
    const update = await ecoleModel.updateOne(
        { _id: req.params.id },
        req.body
    );
    res.send(update);
});

router.delete('/:id', async (req, res) => {
    const deleteRes = await ecoleModel.deleteOne({ _id: req.params.id });
    res.send(deleteRes);
});

module.exports = router;