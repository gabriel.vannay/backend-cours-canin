const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

const coursModel = require('../models/coursModel');

router.get('/', async (req, res) => {
    const cours = await coursModel.find();
    res.send(cours);
    
    router.get('/:id', async (req, res) => {
        const cours = await coursModel.findOne({ _id: req.params.id });
        res.send(cours);
});

router.post('/', async (req, res) => {
    const insert = await coursModel.insertMany(req.body);
    res.send(insert);
});

router.put('/:id', async (req, res) => {
    const update = await coursModel.updateOne(
        { _id: req.params.id },
        req.body
        );
        res.send(update);
    });
    
    router.delete('/:id', async (req, res) => {
        const deleteRes = await coursModel.deleteOne({ _id: req.params.id });
        res.send(deleteRes);
    });
})

router.get('/count', async (req, res) => {
    const cours = await coursModel.aggregate([
        {
            $group: {
                _id: "$niveau",
                count: {
                    $count: {}
                }
            }
        },
        {
            $sort: {
                count: -1,
                _id: 1
            }
        }
    ]);
    res.send(cours);
})

module.exports = router;